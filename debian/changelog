publicfile-installer (0.16) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:48:48 +0200

publicfile-installer (0.15) unstable; urgency=medium

  * UNRELEASED
  * debian/control: update vcs-headers: moved from alioth to salsa.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Thu, 07 Jun 2018 07:31:36 +0200

publicfile-installer (0.14) unstable; urgency=medium

  * Targeting publicfile 0.52-11.  The publicfile 0.52-11 Debian packaging
    comes with various improvements in ad1810-autoindex.  See
    /u/s/d/publicfile/changelog.Debian.gz for all details.
    - publicfile-installer.conf: update publicfile Debian package checksum and
      version.
  * debian/po: Update debconf translations by various contributors.  Thank you,
    tranlators!
    - de.po: German, by Chris Leick.  Closes: #885615
    - ru.po: Russian, by Lev Lamberov.  Closes: #885991

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 12 Feb 2018 18:10:17 +0100

publicfile-installer (0.13) unstable; urgency=medium

  * Targeting publicfile 0.52-10 (missed 0.52-9).  The publicfile 0.52-10
    and -9 Debian packaging comes with support for adjusting the list of
    supported MIME types at run time, and ships with the script
    update-publicfile-contenttype(1) making this easier.  Again,
    documentation has been improved.  See
    /u/s/d/publicfile/changelog.Debian.gz for all details.
  * debian/source/format: converted to Debian native package.
  * debian/po: Update debconf translations by various contributors.  Thank you,
    tranlators!
    - sv.po: Swedish, by Jonatan Nyberg.  Closes: #823657
    - de.po: German, by Chris Leick.  Closes: #843767
    - pt.po: Portuguese, by Américo Monteiro.  Closes: #849133
    - da.po: Danish, by Joe Dalton.  Closes: #849965
    - nl.po: Dutch, by Frans Spiesschaert and the debian-l10n-dutch mailing list
      contributors.  Closes: #850736
    - it.po: Italian, by Beatrice Torracca and the Italian localization team.
      Closes: #872746
  * debian/control: changed Priority from extra to optional (thanks lintian).
  * publicfile-installer.1: get-publicfile needs a public PGP key; document
    this.
  * get-publicfile: remove DIFFFILE and DSCFILE in case these turn out to be
    corrupt: make sure they won't get trusted and reused in a second run of the
    script.
  * build-publicfile: cosmetics, cleanup

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sat, 04 Nov 2017 06:01:08 +0100

publicfile-installer (0.12-1) unstable; urgency=medium

  * New upstream, targeting publicfile (0.52-8).  The publicfile 0.52-8
    Debian packaging comes with improved documentation, an improved
    get-publicfile-docs(1) and some bug fixes.  See
    /u/s/d/publicfile/changelog.Debian.gz for all details.
  * debian/po: Add debconf translations by various contributors.  Thank you,
    tranlators!
    - cs.po: Czech, by Michal Simunek.  Closes: #799055
    - de.po: German, by Chris Leick.  Closes: #799067
    - nl.po: Dutch, by Frans Spiesschaert, with minor modifications by
      Joost van Baal-Ilić.  Closes: #799462
    - pt.po: Portuguese, by Américo Monteiro.  Closes: #799566
    - ru.po: Russian, by Yuri Kozlov.  Closes: #799699
    - it.po: Italian, by Beatrice Torracca and the Italian localization team.
      Closes: #799750
    - da.po: Danish, by Joe Hansen.  Closes: #799877
    - fr.po: French, by jean-pierre giraud and the debian-l10n-french
      mailing list contributors.  Closes: #799882
  * debian/templates, debian/control: Fix language, thanks Justin B Rye and
    debian-l10n-english@l.d.o.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sun, 18 Dec 2016 21:25:27 +0100

publicfile-installer (0.11-1) unstable; urgency=low

  * New upstream.  No longer ships install-publicfile, no longer uses /tmp.
    This fixes a serious security issue: a local privilage escalation
    security hole due to insecure use of /tmp. "This [...] package downloads
    the source code for DJB's publicfile, builds it, and then puts the
    output in a predictable location in a world-writable directory, using an
    existing directory of that name if it already exists, then (either
    automatically or by telling the admin to run another script) installs
    whatever happens to be in that directory.  This can be exploited by
    malicious local users to get arbitrary installscripts executed as root."
    Thanks Justin B Rye.  Closes: #795062.
    + debian/templates: adjusted.
    + debian/control: Depends: add sudo.
  * debian/changelog: fix spelling error.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sun, 06 Sep 2015 07:23:33 +0200

publicfile-installer (0.10-1) unstable; urgency=low

  * New upstream, targeting publicfile (0.52-7).
  * Upload to Debian archive.  Closes: #122614.
  * debian/copyright: add one missing copyright statement, add "Source",
    update license on most files from GPL-2 to GPL-3.
  * debian/control: enhance description.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 20 May 2015 15:46:02 +0100

publicfile-installer (0.9-1) unstable; urgency=low

  * Initial release.
  * debian/control: update standards from 3.7.2 to 3.9.6 (no changes needed).
  * README, debian/*: s/aangifte-ib/publicfile/.
  * debian/rules: new style dh; add override_dh_auto_install.
  * debian/source/format: added; 3.0.
  * debian/{compat,control}: compat: added; 9; control: dh build-depends
    bumped from 5 to 9.
  * debian/install: add *-publicfile scripts.
  * debian/templates: add leading _ to Description in order to aid
    translators; thanks lintian.
  * debian/copyright: link to versioned GPL, now using "Machine-readable
    debian/copyright file" version 1.0.
  * debian/control: add fields Vcs-Git, Vcs-Browser.
  * debian/po/POTFILES.in, debian/{control,rules}: file POTFILES.in added,
    po-debconf build dependency added, call debconf-updatepo in clean target:
    use po-debconf for i18n.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 04 Feb 2015 20:55:16 +0100

aangifte-ib-installer (0.8-1) unstable; urgency=low

  * New upstream.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Thu, 14 Feb 2008 15:37:27 +0100

aangifte-ib-installer (0.7-1) unstable; urgency=low

  * New upstream.
  * rules: do not ignore errors from "make clean" (thanks lintian).

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Wed, 11 Jul 2007 16:12:09 +0200

aangifte-ib-installer (0.6-1) unstable; urgency=low

  * New upstream.
  * Depend upon fakeroot, since build-aangifte-ib calls this explicitly.
    Thanks Bram Senders.
  * control: suggest to keep this package installed.
  * postrm: remove /usr/src/aangifte-ib-installer/ in case of purge.  Thanks
    again Bram Senders.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Tue, 13 Mar 2007 09:50:04 +0100

aangifte-ib-installer (0.5-1) unstable; urgency=low

  * New upstream: bugfix: md5sum of .diff.gz fixed.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Mon, 05 Mar 2007 06:51:36 +0100

aangifte-ib-installer (0.4-1) unstable; urgency=low

  * New upstream: don't use test version, but real version from
    belastingdienst.nl.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Sun, 04 Mar 2007 21:07:58 +0100

aangifte-ib-installer (0.3-1) unstable; urgency=low

  * New upstream: more robust, manpages added.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Wed, 22 Nov 2006 11:10:46 +0100

aangifte-ib-installer (0.2-1) unstable; urgency=low

  * New upstream.
  * Don't try to run dpkg from within dpkg, but postpone installation of
    generated package.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Tue, 21 Nov 2006 15:20:41 +0100

aangifte-ib-installer (0.1-1) unstable; urgency=low

  * Initial release.

 -- Joost van Baal <joostvb-aangifte-ib@mdcc.cx>  Tue, 21 Nov 2006 11:34:24 +0100
